package astra.core;



public abstract class AbstractTask implements Task {
	/**
	 *
	 */
	private static final long serialVersionUID = -289485316850458471L;
	
	long start, duration;
	private boolean finished = false;
	private String name;
	
	public AbstractTask(String name) {
		this.name = name;
	}

	public AbstractTask() {
		this.name = "undefined";
	}

	public String toString() {
		return name;
	}
	
	public abstract void doTask();
	
	public void run() {
		start = System.nanoTime();
		doTask();
		duration = System.nanoTime() - start;
		finished = true;
//		System.out.println("Finished: " + name + " / " + duration + "ns");
	}

	public long duration() {
		return duration;
	}

	public boolean isFinished() {
		return finished;
	}
}
