package astra.statement;

import astra.core.Agent;
import astra.core.Agent.Promise;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import astra.core.Intention;
import astra.formula.Formula;
import astra.reasoner.util.ContextEvaluateVisitor;
import astra.term.Term;

public class MaintainBlock extends AbstractStatement {
	Statement statement;
	Formula formula;

	public MaintainBlock(String clazz, int[] data, Formula formula, Statement statement) {
		this.setLocation(clazz, data[0], data[1], data[2], data[3]);
		this.formula = formula;
		this.statement = statement;
	}

	public StatementHandler getStatementHandler() {
		return new AbstractStatementHandler() {
			int state = 0;
			Promise promise;

			public boolean execute(final Intention intention) {
				switch (state) {
				case 0:
					intention.makePromise(promise = new Promise(true) {
						List<Map<Integer, Term>> bindings;
						Formula guard = (Formula) formula.accept(new ContextEvaluateVisitor(intention));

						public boolean evaluate(Agent agent) {
							bindings = agent.query(guard, new HashMap<Integer, Term>());
							return (bindings == null);
						}

						public void act() {
							intention.failed("Maintenance Condition False: " + formula);
							intention.resume();
						}
					});
					executor.addStatement(statement.getStatementHandler());
					state=1;
					break;
				case 1:
					intention.dropPromise(promise);
					return false;
				}
				return true;
			}

			public boolean onFail(Intention intention) {
				return false;
			}

			public Statement statement() {
				return MaintainBlock.this;
			}
		};
	}

}
