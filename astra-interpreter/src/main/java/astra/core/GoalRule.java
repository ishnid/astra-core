package astra.core;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import astra.event.Event;
import astra.formula.Formula;
import astra.formula.IsDone;
import astra.formula.Predicate;
import astra.reasoner.util.LogicVisitor;
import astra.statement.Statement;

public class GoalRule extends Rule {
	public Formula dropCondition;
	public Map<String, List<Rule>> rules = new HashMap<String, List<Rule>>();
	public Set<String> filter = new HashSet<String>();
	
	public GoalRule(Event event, Statement statement) {
		this(event, Predicate.TRUE, new IsDone(), statement);
	}
	
	public GoalRule(Event event, Formula context, Statement statement) {
		this(event, context, new IsDone(), statement);
	}
	
	public GoalRule(Event event, Formula context, Formula dropCondition, Statement statement) {
		super(event, context, statement);
		this.dropCondition = dropCondition;
	}

	public GoalRule(String clazz, int[] data, Event event, Statement statement) {
		this(clazz, data, event, Predicate.TRUE, new IsDone(), statement);
	}
	
	public GoalRule(String clazz, int[] data, Event event, Formula context, Statement statement) {
		this(clazz, data, event, context, new IsDone(), statement);
	}
	public GoalRule(String clazz, int[] data, Event event, Formula context, Statement statement, Rule[] rules) {
		this(clazz, data, event, context, new IsDone(), statement);
		
		for (int i=0;i<rules.length;i++) {
			addRule(rules[i]);
		}
	}
	
	public GoalRule(String clazz, int[] data, Event event, Formula context, Formula dropCondition, Statement statement) {
		super(clazz, data, event, context, statement);
		this.dropCondition = dropCondition;
	}

	public GoalRule(String clazz, int[] data, Event event, Formula context, Formula dropCondition, Statement statement, Rule[] rules) {
		this(clazz, data, event, context, dropCondition, statement);

		for (int i=0;i<rules.length;i++) {
			addRule(rules[i]);
		}
	}

	public Map<String, List<Rule>> rules() {
		return rules;
	}
	
	public void addRule(Rule rule) {
		List<Rule> list = rules.get(rule.event.signature());

		if (list == null) {
			filter.add(rule.event.signature());
			list = new LinkedList<Rule>();
			rules.put(rule.event.signature(), list);
		}
		
		list.add(rule);
	}

	public Set<String> filter() {
		return filter;
	}

	public String toString() {
		return event.toString() + " : " + context.toString();
	}

	public Rule accept(LogicVisitor visitor) {
		// System.out.println("Copying: " + event);
		// System.out.println("Rules: " + rules);
		GoalRule goalRule = new GoalRule((Event) event.accept(visitor), (Formula) context.accept(visitor), (Formula) dropCondition.accept(visitor), statement);
		for (List<Rule> list : rules.values()) {
			for (Rule rule : list) {
				// System.out.println(">>>>>>>>>>>>>>>>>>>>>> Adding: " + rule.accept(visitor));
				goalRule.addRule((Rule) rule.accept(visitor));
			}
		}
		return goalRule;
	}
}
