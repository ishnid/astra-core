package astra.reasoner;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import astra.formula.AND;
import astra.term.Term;

public class ANDStackEntry implements ReasonerStackEntry {
	AND and;
	int index = 0;
	Queue<Map<Integer, Term>> queue = new LinkedList<Map<Integer, Term>>();
	Queue<Map<Integer, Term>> next = new LinkedList<Map<Integer, Term>>();
	Map<Integer, Term> bindings;
	List<Map<Integer, Term>> solutions = new LinkedList<Map<Integer, Term>>();
	ResolutionBasedReasoner reasoner;
	
	// This has been added because inference rule were not working correctly. in cases where the
	// user is performing a single query that calls an inference rule whose r.h.s. was a conjunction,
	// and there are multiple bindings for the first term, options for this term were being lost
	// even though they should be checked.
	boolean singleResult;
	
	public ANDStackEntry(ResolutionBasedReasoner reasoner,AND and, Map<Integer, Term> bindings) {
		this.and = and;
		this.reasoner = reasoner;
		queue.add(bindings);
	}
	
	@Override
	public boolean solve() {
		singleResult = reasoner.singleResult;
		// Set single result = false here because latter terms can cause earlier terms to fail.
		reasoner.singleResult=false;
		if (index < and.formulae().length) {
			if (!queue.isEmpty()) {
				bindings = queue.remove();
//				System.out.println("\tprocessing: " + and.formulae()[index] + " / bindings: " + bindings);
				try {
					reasoner.stack.push(reasoner.newStackEntry(and.formulae()[index], bindings));
				} catch (NullPointerException e) {
					System.err.println("Formula: " + and.formulae()[index]);
					e.printStackTrace();
				}
			} else {
//				System.out.println("\tnext [" + index + "]: " + next);
				queue = next;
				next = new LinkedList<Map<Integer, Term>>();
				index++;
			}
		} else {
//			System.out.println("\tpropogating final bindings: " + solutions);
			while (!solutions.isEmpty()) {
				reasoner.propagateBindings(solutions.remove(0));
			}
			// undo change to return to previous result mode...
			reasoner.singleResult = singleResult;
			reasoner.stack.pop();
		}
		
		return true;
	}

	public String toString() {
		return "[ANDStackEntry]: " + and.toString();
	}

	@Override
	public boolean failure() {
		// If a single formula is false, then the AND formula is
		// false...
		if (queue.isEmpty() && solutions.isEmpty()) {
//			System.out.println("No more solutions");
			return true;
		}
		
//		System.out.println("[ANDStackEntry]: " + and + " - More solutions to handle...");
		return false;
	}

	@Override
	public void addBindings(Map<Integer, Term> bindings) {
//		System.out.println("[ANDStackEntry]: Propogating Bindings: " + bindings);
		if (index < and.formulae().length-1) {
//			System.out.println("[" + index + "] adding to next:"  + bindings);
			next.add(bindings);
		} else {
//			System.out.println("adding to solutions:"  + bindings);
			solutions.add(bindings);
		}
	}
}
