package astra.event;

import astra.reasoner.util.LogicVisitor;

public class ScopedBeliefEvent implements Event {
	String scope;
	BeliefEvent event;

	public ScopedBeliefEvent(String scope, BeliefEvent event) {
		this.scope = scope;
		this.event = event;
	}

	public String scope() {
		return scope;
	}

	public BeliefEvent beliefEvent() {
		return event;
	}

	public Object getSource() {
		return null;
	}

	public String toString() {
		return scope + "::" + event;
	}

	public String signature() {
		return event.signature();
	}

	@Override
	public Event accept(LogicVisitor visitor) {
		return new ScopedBeliefEvent(scope, (BeliefEvent) event.accept(visitor));
	}
}
