package astra.statement;

import java.util.Queue;

import astra.core.Intention;
import astra.event.GoalEvent;
import astra.formula.Formula;
import astra.formula.Goal;
import astra.reasoner.util.ContextEvaluateVisitor;
import astra.reasoner.util.VariableVisitor;
import astra.term.Variable;

public class Subgoal extends AbstractStatement {
	Goal goal;
	
	public Subgoal(Goal goal) {
		this.goal = goal;
	}
	
	public Subgoal(String clazz, int[] data, Goal goal) {
		setLocation(clazz, data[0], data[1], data[2], data[3]);
		this.goal = goal;
	}
	
	@Override
	public StatementHandler getStatementHandler() {
		return new AbstractStatementHandler() {
			Goal gl;
			
			int index = 0;
			@Override
			public boolean execute(Intention intention) {
				switch (index) {
				case 0:
					// System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
					// System.out.println("goal: " + goal);
					executor = intention.executor();
					// System.out.println("all variables: " +executor.getAllBindings());
					// System.out.println("variables: " +executor.bindings());

					//gl = (Goal) goal.accept(new BindingsEvaluateVisitor(executor.getAllBindings(), intention.agent));
					gl = (Goal) goal.accept(new ContextEvaluateVisitor(intention));
					// System.out.println("Gl:" + gl);
				
					// Get Unbound Variables from Goal
					VariableVisitor visitor = new VariableVisitor();
					gl.accept(visitor);

					for (Variable variable : visitor.variables()) {
						executor.addVariable(variable);
					}

				    intention.addEvent(new GoalEvent(GoalEvent.ADDITION, gl, executor));
					intention.suspend();

					index = 1;
					return true;
				case 1:
					intention.addEvent(new GoalEvent(GoalEvent.REMOVAL, gl));
				}
				return false;
			}

			@Override
			public boolean onFail(Intention context) {
				context.addEvent(new GoalEvent(GoalEvent.REMOVAL, gl));
				return false;
			}
			
			@Override
			public Statement statement() {
				return Subgoal.this;
			}
			
			public String toString() {
				if (gl == null) return goal.toString();
				return gl.toString();
			}
			
			public void addGoals(Queue<Formula> list) {
				list.add(gl);
			}
		};
	}
	

}
