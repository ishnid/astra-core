package astra.core;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;

import astra.event.Event;
import astra.formula.Formula;
import astra.statement.Block;
import astra.statement.StatementHandler;
import astra.term.Term;
import astra.term.Variable;

public class RuleExecutor {
	private Event event;
	private Rule rule;
	private Map<Integer, Term> bindings;
	private Stack<StatementExecutor> executors = new Stack<>();
	private Map<Integer, Term> unbound;
	private RuleExecutor parent;
	private Intention intention;

	public RuleExecutor(Event event, Rule rule, Map<Integer, Term> bindings, RuleExecutor parent, Intention intention) {
		this.event = event;
		this.rule = rule;
		this.bindings = bindings;
		this.parent = parent;
		this.intention = intention;
		if (intention == null) {
			System.out.println("Intention is null: "+ event);
		}

		StatementHandler handler = rule.statement.getStatementHandler();
		handler.setRuleExecutor(this);
		executors.add(new StatementExecutor(handler));
		captureUnboundVariables();
	}

	private void captureUnboundVariables() {
		unbound = new HashMap<>();
		
		for (Entry<Integer, Term> entry : bindings.entrySet()) {
			if (entry.getValue() instanceof Variable) {
				Variable var = ((Variable) entry.getValue());
				unbound.put(var.id(), new Variable(var.type(), entry.getKey()));
			}
		}
	}

	public void addUnboundVariables(Set<Variable> variables) {
		for(Variable variable : variables) {
			if (getValue(variable) == null) {
				addVariable(variable);
			}
		}
	}	
	public Map<Integer, Term> getUnboundBindings() {
		for (Entry<Integer, Term> entry : unbound.entrySet()) {
			if (Variable.class.isInstance(entry.getValue())) 
				unbound.put(entry.getKey(), bindings.get(((Variable) entry.getValue()).id()));
		}
		return unbound;
	}

	public void updateRuleBindings(Map<Integer, Term> bindings) {
		int i=executors.size()-1;
		// System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		// System.out.println("Updating bindings for: " + this.toString());
		while (i >= 0) {
			StatementExecutor executor = executors.get(i--);
			if (executor.bindings() != null) updateBindings(executor.bindings(), bindings);
		}
		// System.out.println("Statement: " + executors.peek().toString());
		// System.out.println("Local Bindings: " + bindings);
		// System.out.println("Stored Bindings: " + this.bindings);
		updateBindings(this.bindings, bindings);
		// System.out.println("Final Rule Bindings: " + this.bindings);
	}

	private void updateBindings(Map<Integer, Term> bindings, Map<Integer, Term> bindings2) {
		for (Entry<Integer, Term> entry:bindings.entrySet()) {
			if (entry.getValue() == null) {
				bindings.put(entry.getKey(), bindings2.get(entry.getKey()));
			}
		}
	}

	public boolean execute(Intention intention) {
		if (executors.isEmpty()) return false;
		// System.out.println("Executing: " + executors.peek());
		if (!executors.peek().execute(intention)) {
			// System.out.println("[" + intention.agent.name()+"] Popping: " + executors.pop());
			executors.pop();
		}

		// if (executors.isEmpty()) System.out.println("[" + intention.agent.name()+"] FINISHED WITH: " + this.event);
		return !executors.isEmpty();
	}

	
	
	public boolean isDone() {
		return executors.isEmpty();
	}

	public void addStatement(StatementHandler handler) {
		handler.setRuleExecutor(this);
		StatementExecutor executor = null;
		if (handler.statement() instanceof Block) {
			executor = new StatementExecutor(handler, new HashMap<>());
		} else {
			executor = new StatementExecutor(handler);
		}
		executors.push(executor);
	}

	public void addStatement(StatementHandler handler, Map<Integer, Term> bindings) {
		handler.setRuleExecutor(this);
		executors.push(new StatementExecutor(handler, bindings));
	}

	public Event event() {
		return event;
	}


	public void addVariable(Variable variable) {
		addVariable(variable, null);
	}

	public void addVariable(Variable variable, Term term) {
		Map<Integer, Term> b = getTopBindings();
		if (b==null) b=bindings;
		b.put(variable.id(), term);
	}
	
	public void removeVariable(Variable variable) {
		Map<Integer, Term> b = getTopBindings();
		if (b==null) b=bindings;
		b.remove(variable.id());
	}

	private Map<Integer, Term> getTopBindings() {
		int i = executors.size()-1;
		while (i >= 0) {
			if (executors.get(i).bindings() != null) {
//				System.out.println("\ttop bindings: (" + i + ") = "+ executors.get(i).bindings());
				return executors.get(i).bindings();
			}
			i--;
		}
		return null;
	}
	
	public boolean updateVariable(Variable term, Term logic) {
		int i=executors.size()-1;
		
		while (i >= 0) {
			if (executors.get(i).bindings() != null
				&& executors.get(i).updateVariable(term, logic)) return true;
			i--;
		}

		if (bindings.containsKey(term.id())) {
			bindings.put(term.id(), logic);
			return true;
		}
		return false;
	}


	public Term getValue(Variable term) {
		int i=executors.size()-1;
		while (i >= 0) {
			StatementExecutor executor = executors.get(i--);
			if((executor.bindings() != null) && executor.bindings().containsKey(term.id())) {
				return executor.bindings().get(term.id());
			}
		}
		
		return bindings.get(term.id());
	}


	public Map<Integer,Term> bindings() {
		return bindings;
	}

	public String toString() {
		return rule.toString();
	}

	public String variableTrace() {
		StringBuilder out = new StringBuilder();
		int i=executors.size()-1;
		while (i >= 0) {
			StatementExecutor executor = executors.get(i--);
			if (executor.bindings() != null) 
				out.append("(").
					append(i+1).
					append("). ").
					append(executor.bindings()).
					append("\n");
		}
		
		out.append("(BASE). ").append(bindings).append("\n");
		return out.toString();
	}

	public StatementHandler getNextStatment() {
		return executors.peek().getStatement();
	}

	public boolean rollback(Intention intention) {
		while (!executors.isEmpty()) {
			StatementExecutor executor = executors.peek();
			if (executor.rollback(intention)) return true;
			executors.pop();
		}
		return false;
	}

	public void buildFailureTrace(Stack<StatementHandler> failureTrace) {
		for (StatementExecutor executor : executors) {
			executor.buildFailureTrace(failureTrace);
		}
	}

	public void addBindings(Map<Integer, Term> bindings) {
		Map<Integer, Term> b = getTopBindings();
		if (b==null) b=this.bindings;
		for (Entry<Integer, Term> entry : bindings.entrySet()) {
			if (!b.containsKey(entry.getKey())) {
				b.put(entry.getKey(), entry.getValue());
			}
		}
	}

	public Rule rule() {
		return rule;
	}

	public void printStackTrace() {
		System.out.println("stack size: " + executors.size());
		for(int i=executors.size()-1;i>=0;i--) {
			System.out.println("("+i+") " + executors.get(i).toString());
		}
		
	}

	public void addGoals(Queue<Formula> queue) {
		// NOTE: size-1 used here as the last item in the stack
		// should not be considered (it could be a goal that has
		// been adopted but not yet matched to a plan)
		for (int i=0;i<executors.size()-1;i++) {
			executors.get(i).addGoals(queue);
		}
	}

	public RuleExecutor parent() {
		return parent;
	}

	public Map<Integer, Term> getAllBindings() {
		Map<Integer, Term> lBindings = new HashMap<>();
		lBindings.putAll(this.bindings);
		for (StatementExecutor executor : executors) {
			Map<Integer, Term> b = executor.bindings();
			if (b != null) {
				lBindings.putAll(b);
			}
		}
//		if (parent != null) bindings.putAll(parent.bindings);
		return lBindings;
	}

	public Intention intention() {
		return intention;
	}
}
