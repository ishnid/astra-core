package astra.reasoner;

import java.util.Map;

import astra.formula.Formula;
import astra.formula.Goal;
import astra.term.Term;

public class GoalStackEntryFactory implements ReasonerStackEntryFactory {
	@Override
	public ReasonerStackEntry create(ResolutionBasedReasoner reasoner, Formula formula, Map<Integer, Term> bindings) {
		return new GoalStackEntry(reasoner, (Goal) formula, bindings);
	}

}
