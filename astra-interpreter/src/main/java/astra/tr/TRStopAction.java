package astra.tr;

import java.util.Map;

import astra.term.Term;

public class TRStopAction extends AbstractAction {
	public ActionHandler getStatementHandler() {
		return new ActionHandler() {
			public boolean execute(TRContext context, Map<Integer, Term> bindings) {
//				System.out.println("stopping...");
				context.stopFunction();
				return false;
			}
		};
	}

}
