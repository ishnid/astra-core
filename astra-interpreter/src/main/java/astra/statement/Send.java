package astra.statement;

import java.util.HashMap;

import astra.core.Intention;
import astra.formula.Formula;
import astra.formula.ModuleFormula;
import astra.messaging.AstraMessage;
import astra.messaging.MessageService;
import astra.reasoner.util.BindingsEvaluateVisitor;
import astra.reasoner.util.ContextEvaluateVisitor;
import astra.term.FormulaTerm;
import astra.term.Funct;
import astra.term.ListTerm;
import astra.term.Performative;
import astra.term.Primitive;
import astra.term.Term;

public class Send extends AbstractStatement {
	Term performative;
	Term name;
	Formula content;
	Term params;
	
	public Send(String clazz, int[] data, Term performative, Term name, Formula content) {
		this(clazz, data, performative, name, content, null);
	}

	public Send(String clazz, int[] data, Term performative, Term name, Formula content, Term params) {
		setLocation(clazz, data[0], data[1], data[2], data[3]);
		this.performative = performative;
		this.name = name;
		this.content = content;
		this.params = params;
	}

	public StatementHandler getStatementHandler() {
		return new AbstractStatementHandler() {
			@SuppressWarnings("unchecked")
			public boolean execute(Intention intention) {
				try {
					ContextEvaluateVisitor visitor = new ContextEvaluateVisitor(intention); 
					AstraMessage message = new AstraMessage();
					message.sender = intention.name();
					Term receiver = (Term) name.accept(visitor);

					if (Primitive.class.isInstance(receiver)) {
						message.receivers.add(((Primitive<String>) receiver).value());
					} else {
						for (Term term : (ListTerm) receiver) {
							message.receivers.add(((Primitive<String>) term).value());
						}
					}
					
					if (params != null) {
						Object parameters = params.accept(visitor);
						if (ListTerm.class.isInstance(parameters)) {
	//						System.out.println("params: " + parameters);
							for (Term t : (ListTerm) parameters) {
								if (Funct.class.isInstance(t)) {
									Funct funct = (Funct) ((Funct) t).accept(visitor);
									if (funct.size() > 1) {
										intention.failed("Unexpected Param in send(...): " + funct);
										return false;
									}
									
									if (funct.functor().equals("protocol")) {
										message.protocol = ((Primitive<?>) funct.termAt(0)).value().toString();
									} else if (funct.functor().equals("conversation_id")) {
										message.conversationId = ((Primitive<?>) funct.termAt(0)).value().toString();
									} else {
										intention.failed("Unexpected Param in send(...): " + funct);
										return false;
									}
								}
							}
						}
					}

					BindingsEvaluateVisitor bvisitor = new BindingsEvaluateVisitor(executor.bindings(), intention.agent);
					message.performative = ((Performative) performative.accept(visitor)).value();
					
					// Process the content...
					Formula ctnt = null;
					Object cnt = ((Formula) content.accept(bvisitor)).accept(visitor);
					if (cnt instanceof FormulaTerm) {
						ctnt = ((FormulaTerm) cnt).value();
					} else if (cnt instanceof ModuleFormula) {
						// This is normally executed by the resolution algorithm, but that is
						// not used here. This replaces content defined as a module formula
						// with the actual content.
						ctnt = ((ModuleFormula) cnt).adaptor().invoke(new BindingsEvaluateVisitor(new HashMap<>(), intention.agent), ((ModuleFormula) cnt).predicate());
					} else if (cnt instanceof Formula) {
						ctnt = (Formula) cnt;
					} else {
						intention.failed("Invalid Formula type: " + cnt.getClass().getCanonicalName());
						return false;
					}

					message.content = ctnt;

					if (!MessageService.send(message)) {
						System.out.println("Problem sending: " + message);
						intention.failed("Failed to send message");
					}
				} catch (Exception th) {
					intention.failed("Unexpected Error", th);
				}
				return false;
			}

			public boolean onFail(Intention context) {
				return false;
			}

			public Statement statement() {
				return Send.this;
			}

			public String toString() {
				return "send(" + performative + "," + name + "," + content + ")";
			}
		};
	}
}
