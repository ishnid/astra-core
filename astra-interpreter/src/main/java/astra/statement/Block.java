package astra.statement;

import astra.core.Intention;

public class Block extends AbstractStatement {
	Statement[] statements;
	
	public Block(Statement[] statements) {
		this.statements = statements;
	}

	public Block(String clazz, int data[], Statement[] statements) {
		this.setLocation(clazz, data[0], data[1], data[2], data[3]);
		this.statements = statements;
	}

	public StatementHandler getStatementHandler() {
		return new AbstractStatementHandler() {
			int index = 0;
			
			public boolean execute(Intention intention) {
				if (index < statements.length) {
					StatementHandler handler = statements[index++].getStatementHandler();
					handler.setRuleExecutor(executor);
					executor.addStatement(handler);
					// System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
					// System.out.println("Intention: " + intention.event);
					// System.out.println("Executing: " +executor.event());
					executor.execute(intention);
					return true;
				}
				return false;
			}

			@Override
			public boolean onFail(Intention intention) {
				return false;
			}

			@Override
			public Statement statement() {
				return Block.this;
			}

			public String toString() {
				return "block: " + index + " of " + statements.length;
			}
		};
	}
	
	public String toString() {
		String out = "{\n";
		for (int i=0; i<statements.length;i++) {
			out += statements[i].toString()+"\n";
		}
		return out+"}";
	}
}
