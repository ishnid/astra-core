package astra.term;

import astra.reasoner.util.LogicVisitor;
import astra.type.Type;

public class FromJson implements Term {
	/**
	 *
	 */
	private static final long serialVersionUID = 6908458811392703018L;
	Term term;
	String type;

	public FromJson(Term term, String type) {
		this.term = term;
		this.type = type;
	}
	
	public Type type() {
		return Type.OBJECT;
	}

	public Object accept(LogicVisitor visitor) {
		return visitor.visit(this);
	}

	public boolean matches(Term right) {
		if (!FromJson.class.isInstance(right)) return false;
		return term.matches(((FromJson) right).term);
	}

	public boolean equals(Object object) {
		return (object instanceof FromJson);
	}

	public String signature() {
		return "FJ:"+term.signature();
	}
	
	public FromJson clone() {
		return this;
	}

	public Term term() {
		return term;
	}

	public String toString() {
		return "from_json("+term+","+ type+")";
	}

	public Class<?> getSpecifiedType() {
		try {
			return Class.forName(type);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	public String rawType() {
		return type;
	}
}
