package astra.statement;

import java.util.Queue;

import astra.core.Intention;
import astra.core.RuleExecutor;
import astra.formula.Formula;

public interface StatementHandler {
	public boolean execute(Intention Intention);

	public boolean onFail(Intention intention);

	public Statement statement();

	public void addGoals(Queue<Formula> list);
	
	public void setRuleExecutor(RuleExecutor executor);
}
