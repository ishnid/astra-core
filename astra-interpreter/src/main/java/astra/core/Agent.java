package astra.core;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import astra.event.Event;
import astra.event.GoalEvent;
import astra.event.ScopedBeliefEvent;
import astra.event.ScopedGoalEvent;
import astra.formula.Formula;
import astra.formula.Goal;
import astra.formula.Predicate;
import astra.formula.ScopedGoal;
import astra.messaging.AstraMessage;
import astra.messaging.MessageEvent;
import astra.reasoner.Queryable;
import astra.reasoner.Reasoner;
import astra.reasoner.ResolutionBasedReasoner;
import astra.term.Funct;
import astra.term.ListTerm;
import astra.term.Performative;
import astra.term.Primitive;
import astra.term.Term;
import astra.tr.Function;
import astra.tr.TRContext;
import astra.trace.TraceEvent;
import astra.trace.TraceManager;

public class Agent implements Queryable {
	public static final Map<String, Long> timings = Collections.synchronizedMap(new HashMap<String, Long>());
	public static final Map<String, Long> iterations = Collections.synchronizedMap(new HashMap<String, Long>());
	
	/**
	 * This class models the notifications that are generated when an asynchronously executed action
	 * completes. Receipt of an instance of this class allows the agent to resume or fail the associated
	 * intention.
	 * 
	 * @author Rem Collier
	 *
	 */
	public static class Notification {
		Intention intention;
		String message;
		Throwable th;

		public Notification(Intention intention, String message) {
			this.intention = intention;
			this.message = message;
		}

		public Notification(Intention Intention, String message, Throwable th) {
			this.intention = Intention;
			this.message = message;
			this.th = th;
		}
		
		public void evaluate() {
			if (message != null) {
				intention.failed(message, th);
			}
			intention.resume();
		}
	}
	
	/**
	 * Promises are used to implement WAIT statements. When one of these statements is
	 * executed, the agent creates a promise and suspends the intention. Promises are
	 * evaluated on each iteration. When a promise is fulfilled (i.e. the associated 
	 * formula is matched), the agent executes the associated act(ion) which typically
	 * resumes the intention.
	 * 
	 * @author Rem
	 *
	 */
	public abstract static class Promise {
		public boolean isTrue;

		public Promise() {
			this(false);
		}

		public Promise(boolean isTrue) {
			this.isTrue = isTrue;
		}

		public boolean evaluatePromise(Agent agent) {
			boolean evaluated = evaluate(agent);
			return (isTrue && !evaluated) || (!isTrue && evaluated);
		}

		public abstract boolean evaluate(Agent agent);
		public abstract void act();
	}

	// Agent Registry
	private static Map<String, Agent> agents = new HashMap<>();

	public static Agent getAgent(String name) {
		return agents.get(name);
	}

	public static boolean hasAgent(String name) {
		return agents.containsKey(name);
	}
	
	public static Set<String> agentNames() {
		return agents.keySet();
	}

	// Agent States
	public static final int NEW 									= 0;
	public static final int ACTIVE									= 1;
	public static final int INACTIVE								= 2;
	public static final int RESCHEDULE								= 3;
	public static final int TERMINATING								= 4;
	public static final int TERMINATED 								= 5;
	
	private String name;
	private int state = NEW;
	private Intention intention;
	
	// Trace variable is used to indicate whether or not ignored events should be logged.
	private boolean trace = false;
	
	// Synchronization Fields
	private Set<String> tokens = new TreeSet<>();
    private Map<String, LinkedList<Intention>> lockQueueMap = new TreeMap<>();
    private Map<String, Intention> lockMap = new TreeMap<>();
	
    // Event Queue
	private Set<String> filter = new TreeSet<>();
	private BlockingQueue<Event> eventQueue = new LinkedBlockingQueue<>();
	
	// Intention Management
	private BlockingQueue<Notification> completed = new LinkedBlockingQueue<>();
	private List<Intention> intentions = Collections.synchronizedList(new ArrayList<>());
    
	// Activated TR Function / null if no function active
	private Predicate trFunction;

	// Class Hierarchy
    private ASTRAClass clazz;
	private Map<String, Fragment> linearization = new TreeMap<>();

	// Reasoning Engine
	private Reasoner reasoner;
	private EventBeliefManager beliefManager;
	private List<Promise> promises = new ArrayList<>();
	
	private List<SensorAdaptor> sensorArray = new LinkedList<>();

	// Message Listeners - listener pattern to notify other classes of
	// incoming/outgoing messages.
	private List<AgentMessageListener> messageListeners = new LinkedList<>();
	
	public Agent(String name) {
		this.name = name;

		// initialize the timings table
		timings.put(name, 0l);
		iterations.put(name, 0l);
		
		beliefManager = new EventBeliefManager(this); 
		reasoner = new ResolutionBasedReasoner(this);
		reasoner.addSource(beliefManager);
		reasoner.addSource(this);
		agents.put(name, this);
		
		TraceManager.getInstance().recordEvent(new TraceEvent(TraceEvent.NEW_AGENT, Calendar.getInstance().getTime(), this));
	}
	
	public void addSource(Queryable source) {
		reasoner.addSource(source);
	}
	
	public String name() {
		return name;
	}
	
	public void setMainClass(ASTRAClass clazz) throws ASTRAClassNotFoundException {
		this.clazz = clazz;

		List<ASTRAClass> list = clazz.getLinearization();
        for ( ASTRAClass claz : list ) {
            filter.addAll( claz.filter() );
            reasoner.addSource(claz);
        }
        
        Fragment prev = null;      
        for ( ASTRAClass claz : list ) {
        	claz.initialize(this);
        	Fragment fragment = claz.createFragment(this);
        	if (prev!=null) {
        		prev.next = fragment;
        	}
        	linearization.put(claz.getClass().getCanonicalName(), fragment);
        	prev = fragment;
        }
	}
	
	private List<ASTRAClass> filteredClassList(List<ASTRAClass> classList, String scope) throws ASTRAClassNotFoundException {
		for (ASTRAClass c : classList) {
			if (c.getCanonicalName().equals(scope) || c.getClass().getSimpleName().equals(scope)) {
				return c.getLinearization();
			}
		}
		return null;
	}

	public boolean handleEvent(Event event) {
		if (trace) System.out.println("__________________________________________________________________________");
		if (trace) System.out.println("["+name+"] Handling: "+event);
		
		// Start by checking if the event should be handled through any active plan
		boolean handled = false;
		Object source = event.getSource();
		if (source != null) {
			if (Intention.class.isInstance(source)) {
				if (trace) System.out.println("Intention focused event: " + event);
				if (trace) System.out.println("Intention: " + intention.event);
				handled = ((Intention) source).handleEvent(event, this);
			} else if (RuleExecutor.class.isInstance(source)) {
				if (trace) System.out.println("Intention (Rule) focused event: " + event);
				if (trace) System.out.println("Intention: " + ((RuleExecutor) source).intention().event);
				handled = ((RuleExecutor) source).intention().handleEvent(event, this);
			}
		} else {
			for (int i=0; i < intentions.size(); i++) {
				if (trace) System.out.println("Checking Intentions: "+intentions.get(i).event);
				if (intentions.get(i).handleEvent(event, this)) handled = true;
			}
		}

		if (!handled) {
			if (trace) System.out.println("Checking Root Context: " + event);
			// If we did not handle it in the intention, examine the root context...
			try {
				List<ASTRAClass> classList = clazz.getLinearization();
				
				if (event instanceof ScopedGoalEvent) {
					classList = filteredClassList(classList, ((ScopedGoalEvent) event).scopedGoal().scope());
				}

				if (event instanceof ScopedBeliefEvent) {
					classList = filteredClassList(classList, ((ScopedBeliefEvent) event).scope());
					event = ((ScopedBeliefEvent) event).beliefEvent();
				}

				int i=0;
				while (!handled && i < classList.size()) {
					ASTRAClass cls = classList.get(i++);
					Fragment fragment = linearization.get(cls.getClass().getCanonicalName());
					if (fragment.getASTRAClass().handleEvent(event, this)) handled = true;
				}
			} catch (ASTRAClassNotFoundException e) {
				System.err.println("Problem generating linearisation of: " + clazz.getClass().getCanonicalName());
				e.printStackTrace();
			}
		}

		if (!handled) {
			// Still not handled, so we generate an error!
			if (source != null) {
				Intention lIntention = null;
				if (Intention.class.isInstance(source)) {
					lIntention = (Intention) source;
				} else if (RuleExecutor.class.isInstance(source)) {
					lIntention = ((RuleExecutor) source).intention();
				}
				
				if (trace) System.out.println("Event was not matched to rule: " + event);
				lIntention.failed("Event was not matched to rule: " + event, null);
				lIntention.resume();
			} else {
				if (trace) System.out.println("[astra.core.Agent:"+name+"] Event: " + event +" was not handled");
			}
		}

		if (trace) System.out.println("__________________________________________________________________________");
		return handled;
	}

	public void execute() {
		// if (trace) System.out.println("Executing: " + name);
		long start = System.currentTimeMillis();
		for (SensorAdaptor adaptor : sensorArray) {
			adaptor.sense(this);
		}
		// if (trace) System.out.println("Sensed: " + name);
		
		this.beliefManager.update();
		// if (trace) System.out.println("Updated: " + name);

		for (int i=0; i<promises.size(); i++) {
			if (promises.get(i).evaluatePromise(this)) {
				promises.remove(i).act();
			}
		}

		// if (trace) System.out.println("Handled Promises: " + name);
		
		// Check for notifications of completed actions
		while (!completed.isEmpty()) {
			completed.poll().evaluate();
		}
		
		// HANDLE DROP CONDITIONS FOR ACTIVE INTENTIONS
		// This is done here because you don't want to waste resource
		// evaluating an event w.r.t. an intention if it is to be dropped...
		int i=0;
		while (i < intentions.size()) {
			intention = intentions.get(i);
			if (intention.reviseGoals()) {
				intentions.remove(i);
			} else {
				i++;
			}
		}
		
		synchronized (this) {
			while (!eventQueue.isEmpty() && !handleEvent(eventQueue.poll()));
		}

		if (!intentions.isEmpty()) {
			intention = getNextIntention();
			if (intention != null) {
				if (intention.isFailed()) {
					if (!intention.rollback()) {
						intentions.remove(intention);
						intention.printStackTrace();
					}
				} else {
					if (!intention.execute()) {
						intentions.remove(intention);
					}
				}
			}
		}

		// if (trace) System.out.println("Executed Intention: " + name);
	
		// Execute active functions
		if (trFunction != null) {
			new TRContext(this, trFunction).execute();
		}
		
		TraceManager.getInstance().recordEvent(new TraceEvent(TraceEvent.END_OF_CYCLE, Calendar.getInstance().getTime(), this));
		
		// Record Interpreter Timings
		long duration = System.currentTimeMillis()-start;
//		if(duration > 0) {
			timings.put(name, timings.get(name) + duration);
			iterations.put(name, iterations.get(name) + 1);
//		}
	}
	
	private Intention getNextIntention() {
		if (intentions.isEmpty()) return null;

		Intention intent = null;
		
		int i=0;
		boolean selected = false;
		while (i < intentions.size() && !selected) {
			intent = intentions.remove(0);
			intentions.add(intent);
			selected = !intent.isSuspended();
			i++;
		}
		return intent;
	}

	public List<Map<Integer, Term>> query(Formula formula, Map<Integer, Term> bindings) {
		return reasoner.query(formula, bindings);
	}		

	public List<Map<Integer, Term>> queryAll(Formula formula) {
		return reasoner.queryAll(formula);
	}


	public void initialize(Goal goal) {
		eventQueue.add(new GoalEvent('+', goal));
	}

	public void initialize(ScopedGoal goal) {
		eventQueue.add(new ScopedGoalEvent('+', goal));
	}

	public void initialize(Predicate predicate) {
		beliefManager.addBelief(predicate);
	}

	public void addIntention(Intention intention) {
		intentions.add(0, intention);
	}

	public Module getModule(String classname, String key) {
		Fragment fragment = linearization.get(classname == null ? this.clazz.getCanonicalName():classname);
		for (ASTRAClass claz : fragment.getLinearization()) {
			fragment = linearization.get(claz.getClass().getCanonicalName());
			Module module = fragment.getModule(key);
			if (module != null) {
            	return module;
            }
		}
		return null;
	}

	public EventBeliefManager beliefs() {
		return beliefManager;
	}

	public void receive(AstraMessage message) {
		for(AgentMessageListener listener : messageListeners) {
			listener.receive(message);
		}
		
        // rebuild params...
        ListTerm list = new ListTerm();
        if (message.protocol != null) {
        	list.add(new Funct("protocol", new Term[] {Primitive.newPrimitive(message.protocol) }));
        }
        if (message.conversationId != null) {
        	list.add(new Funct("conversation_id", new Term[] {Primitive.newPrimitive(message.conversationId) }));
        }

		addEvent( new MessageEvent( new Performative(message.performative), Primitive.newPrimitive( message.sender ), (Formula) message.content, list ) );
    }
	
	public synchronized boolean addEvent(Event event) {
		if (checkEvent(event)) {
			eventQueue.add(event);
			
			if (trace) System.out.println("[" + this.name + "] handling event: " + event);
			Scheduler.resumeIfWaiting(this);
			return true;
		}
		if (trace) System.err.println("["+getClass().getCanonicalName()+":" + name + "] Ignoring event: " + event);
		
		return false;
	}

	private synchronized boolean checkEvent(Event event) {
		// Class Level check
		if (filter.contains(event.signature())) {
			return true;
		}
		
		// Intention level check
		Object source = event.getSource();
		if (source != null) {
			if (Intention.class.isInstance(source)) {
				return ((Intention) source).checkEvent(event);
			} else if (RuleExecutor.class.isInstance(source)) {
				return ((RuleExecutor) source).intention().checkEvent(event);
			}
		} else {
			for (int i=0; i < intentions.size(); i++) {
				if (intentions.get(i).checkEvent(event)) return true;
			}
		}
		return false;
	}

	public List<Intention> intentions() {
		return intentions;
	}

	public void addSensorAdaptor(SensorAdaptor adaptor) {
		sensorArray.add(adaptor);
	}
	
	public void notifyDone(Notification notification) {
		synchronized (completed) {
			completed.add(notification);
		}
	}

	public void schedule(Task task) {
		Scheduler.schedule(task);
	}

    public synchronized boolean hasLock( String token, Intention Intention ) {
        return Intention.equals( lockMap.get( token ) );
    }

    public synchronized boolean requestLock( String token, Intention Intention ) {
        if ( tokens.contains( token ) ) {
            // No lock, so queue it..
            lockQueueMap.get( token ).addLast( Intention );
            Intention.suspend();
            return false;
        }

        tokens.add( token );
        lockQueueMap.put( token, new LinkedList<>() );
        lockMap.put( token, Intention );
        return true;
    }

    public synchronized void releaseLock( String token, Intention Intention) {
        if ( !tokens.contains( token ) ) {
            System.err.println( "[" + name() + "] Could not release lock on token: " + token );
        } else {
            if ( !lockMap.remove( token ).equals( Intention ) ) {
                System.out.println( "[ASTRAAgent.releaseLock()] Something strange: look at lock releasing" );
            }

            LinkedList<Intention> queue = lockQueueMap.get( token );
            if ( queue.isEmpty() ) {
                tokens.remove( token );
            }
            else {
                Intention ctxt = queue.removeFirst();
                lockMap.put( token, ctxt );
                ctxt.resume();
            }
        }
    }

	public void unrequestLock(String token, Intention Intention) {
        if ( !tokens.contains( token ) ) {
            System.err.println( "[" + name() + "] Could not unrequest lock on token: " + token );
        } else {
	        LinkedList<Intention> queue = lockQueueMap.get( token );
	        queue.remove(Intention);
	        if ( queue.isEmpty() ) {
	            tokens.remove( token );
	        }
        }		
	}
	
	public synchronized void state(int state) {
		this.state = state;
	}
	
	public synchronized int state() {
		return state;
	}
	
	public synchronized boolean isActive() {
		if (!clazz.hasFunctions()) {
			System.out.println("sa: " + sensorArray.isEmpty());
			return !eventQueue.isEmpty() || !intentions.isEmpty() || (!sensorArray.isEmpty()) || beliefManager.hasUpdates() || (trFunction != null);
		}
		return true;
	}

	public synchronized void terminate() {
		state = TERMINATING;
		agents.remove(name);
	}
	
	public synchronized boolean isTerminating() {
		return state == TERMINATING;
	}
	
	public Queue<Event> events() {
		return eventQueue;
	}

	public boolean startFunction(Predicate function) {
		if (trFunction != null) return false;
		trFunction = function;
		return true;
	}

	public boolean stopFunction() {
		if (trFunction == null) return false;
		trFunction = null;
		return true;
	}

	public Function getFunction(Predicate predicate) {
		Function function;
		Fragment fragment = linearization.get(clazz.getClass().getCanonicalName());
		while (fragment != null) {
			function = fragment.getASTRAClass().getFunction(predicate);
            if (function != null) {
            	return function;
            }
			fragment = fragment.next;
		}
		return null;
	}

	public ASTRAClass getASTRAClass() {
		return this.clazz;
	}

	public Intention intention() {
		return intention;
	}

	public void addAgentMessageListener(AgentMessageListener listener) {
		messageListeners.add(listener);
	}

	public void addPromise(Promise promise) {
		promises.add(promise);
	}

	public void dropPromise(Promise promise) {
		promises.remove(promise);
	}

	public boolean hasActiveFunction() {
		return this.trFunction != null;
	}

	public boolean hasSensors() {
		return !sensorArray.isEmpty();
	}
	
	public void setTrace(boolean trace) {
		this.trace = trace;
	}

	public void addMatchingFormulae(Queue<Formula> queue, Formula predicate) {
		for (Intention lIntention : intentions) {
			lIntention.addGoals(queue);
		}
	}

	public boolean trace() {
		return trace;
	}
}
