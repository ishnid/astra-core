package astra.event;

import astra.reasoner.util.LogicVisitor;

public interface Event {
	public static final char ADDITION = '+';
	public static final char REMOVAL = '-';
	
	public Object getSource();
	public String signature();

	public Event accept(LogicVisitor visitor);
}
