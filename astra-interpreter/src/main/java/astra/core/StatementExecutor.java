package astra.core;

import java.util.Map;
import java.util.Queue;
import java.util.Stack;

import astra.debugger.Breakpoints;
import astra.formula.Formula;
import astra.statement.StatementHandler;
import astra.term.Term;
import astra.term.Variable;

public class StatementExecutor {
	private Map<Integer, Term> bindings;
	private StatementHandler handler;
	
	public StatementExecutor(StatementHandler handler, Map<Integer, Term> bindings) {
		this.bindings = bindings;
		this.handler = handler;
	}

	public StatementExecutor(StatementHandler handler) {
		this(handler, null);
	}

	public boolean execute(Intention intention) {
		Breakpoints.getInstance().check(intention.agent, handler.statement());
		return handler.execute(intention);
	}

	public Map<Integer, Term> bindings() {
		return bindings;
	}

	public boolean updateVariable(Variable term, Term logic) {
		if (bindings.containsKey(term.id())) {
			bindings.put(term.id(), logic);
			return true;
		}
		return false;
	}

	public StatementHandler getStatement() {
		return handler;
	}

	public boolean rollback(Intention intention) {
		return handler.onFail(intention);
	}

	public void buildFailureTrace(Stack<StatementHandler> failureTrace) {
		failureTrace.push(handler);
	}
	
	public String toString() {
		return handler.toString();
	}

	public void addGoals(Queue<Formula> queue) {
		handler.addGoals(queue);
		
	}
}
