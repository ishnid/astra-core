package astra.term;

import astra.reasoner.util.LogicVisitor;
import astra.type.Type;

public class AtIndex implements Term {
	/**
	 *
	 */
	private static final long serialVersionUID = 8787586979212519525L;
	Term term;
	Term index;
	Type type;
	
	public AtIndex(Term term, Term index, Type type) {
		this.term = term;
		this.index = index;
		this.type = type;
	}
	
	public Type type() {
		return type;
	}

	public Object accept(LogicVisitor visitor) {
		return visitor.visit(this);
	}

	public boolean matches(Term right) {
		if (!AtIndex.class.isInstance(right)) return false;
		return term.matches(((AtIndex) right).term);
	}

	public boolean equals(Object object) {
		return (object instanceof AtIndex);
	}

	public String signature() {
		return "HD:"+term.signature();
	}
	
	public AtIndex clone() {
		return this;
	}

	public Term term() {
		return term;
	}

	public Term index() {
		return index;
	}
	
	public String toString() {
		return "at_index("+term+","+index+"," + type+")";
	}

}
