package astra.execution;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import astra.core.Agent;
import astra.core.Scheduler;
import astra.core.Task;

public class AdaptiveSchedulerStrategy implements SchedulerStrategy {
	private ExecutorService executor = Executors.newFixedThreadPool(5);
	private Map<String, Integer> agents = new HashMap<String, Integer>();
	
	public void schedule(final Agent agent) {
		Integer state = null;
		// synchronized (this) {
			state = agents.get(agent.name());
			if (state == null) {
				agents.put(agent.name(), state = Scheduler.ACTIVE);
			// }
		}
		
		if (state == Scheduler.ACTIVE) {
			executor.submit(new Runnable() {
				public void run() {
					try {
						agent.execute();
					} catch (Throwable th) {
						System.err.println("Major Error in Agent: " + agent.name());
						th.printStackTrace();
						System.exit(0);
					}
					
					if (isActive(agent)) {
						schedule(agent);
					} else {
						agents.put(agent.name(), Scheduler.WAITING);
//						System.out.println("SUSPENDING: " + agent.name());
					}
				}
			});
		}
	}

	private boolean isActive(Agent agent) {
		return (agent.hasSensors()) || !(agent.events().isEmpty() && agent.intentions().isEmpty()) || agent.hasActiveFunction();
	}
	
	public void schedule(Task task) {
		executor.submit(task);
	}

	public void setThreadPoolSize(int size) {
		ExecutorService oldExecutor = executor;
		executor = Executors.newFixedThreadPool(size);
		oldExecutor.shutdown();
	}

	public void stop() {
	}

	public void setState(Agent agent, int state) {
		agents.put(agent.name(), state);
	}

	public synchronized int getState(Agent agent) {
		if (!agents.containsKey(agent.name())) return Scheduler.ACTIVE;
		return agents.get(agent.name());
	}

	public void setSleepTime(long sleepTime) {
	}

	public void shutdown() {
		executor.shutdownNow();
	}

}
