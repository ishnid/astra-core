# astra-interpreter

ASTRA is an AOP language that is based on AgentSpeak(TR+ER): the basic AgentSpeak(L)
language plus a Teleo-Reactive component and Encapsulated (Goal) Rules.

This repository contains the implementation of the core interpreter for ASTRA and is
available as a maven repository.