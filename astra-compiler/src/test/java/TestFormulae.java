import astra.ast.core.*;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;


public class TestFormulae {
	ASTRAParser parser;
    ADTTokenizer tokenizer;
	
    public List<Token> setup(String input) throws ParseException {
        tokenizer = new ADTTokenizer(new ByteArrayInputStream(input.getBytes()));
        parser = new ASTRAParser(tokenizer);
		List<Token> list = new ArrayList<Token>();
		Token token = tokenizer.nextToken();
		while (token != Token.EOF_TOKEN) {
			list.add(token);
			token = tokenizer.nextToken();
		}
        
        return list;
    }

	// @Test
	// public void stringVarTest() throws ParseException {
	// 	List<Token> tokens = setup("string X");
	// 	ITerm term = parser.createTerm(tokens);
	// 	assertEquals(InlineVariableDeclaration.class, term.getClass());
	// 	varComparison((InlineVariableDeclaration) term, new BasicType(Token.STRING), "X");
	// }

	// @Test
	// public void intVarTest() throws ParseException {
	// 	List<Token> tokens = setup("int X");
	// 	ITerm term = parser.createTerm(tokens);
	// 	assertEquals(InlineVariableDeclaration.class, term.getClass());
	// 	varComparison((InlineVariableDeclaration) term, new BasicType(Token.INTEGER), "X");
	// }

	// @Test
	// public void longVarTest() throws ParseException {
	// 	List<Token> tokens = setup("long X");
	// 	ITerm term = parser.createTerm(tokens);
	// 	assertEquals(InlineVariableDeclaration.class, term.getClass());
	// 	varComparison((InlineVariableDeclaration) term, new BasicType(Token.LONG), "X");
	// }

	// @Test
	// public void floatVarTest() throws ParseException {
	// 	List<Token> tokens = setup("float X");
	// 	ITerm term = parser.createTerm(tokens);
	// 	assertEquals(InlineVariableDeclaration.class, term.getClass());
	// 	varComparison((InlineVariableDeclaration) term, new BasicType(Token.FLOAT), "X");
	// }

	// @Test
	// public void doubleVarTest() throws ParseException {
	// 	List<Token> tokens = setup("double X");
	// 	ITerm term = parser.createTerm(tokens);
	// 	assertEquals(InlineVariableDeclaration.class, term.getClass());
	// 	varComparison((InlineVariableDeclaration) term, new BasicType(Token.DOUBLE), "X");
	// }

	// @Test
	// public void charVarTest() throws ParseException {
	// 	List<Token> tokens = setup("char X");
	// 	ITerm term = parser.createTerm(tokens);
	// 	assertEquals(InlineVariableDeclaration.class, term.getClass());
	// 	varComparison((InlineVariableDeclaration) term, new BasicType(Token.CHARACTER), "X");
	// }

	// @Test
	// public void booleanVarTest() throws ParseException {
	// 	List<Token> tokens = setup("boolean X");
	// 	ITerm term = parser.createTerm(tokens);
	// 	assertEquals(InlineVariableDeclaration.class, term.getClass());
	// 	varComparison((InlineVariableDeclaration) term, new BasicType(Token.BOOLEAN), "X");
	// }

	// @Test
	// public void functVarTest() throws ParseException {
	// 	List<Token> tokens = setup("funct X");
	// 	ITerm term = parser.createTerm(tokens);
	// 	assertEquals(InlineVariableDeclaration.class, term.getClass());
	// 	varComparison((InlineVariableDeclaration) term, new BasicType(Token.FUNCT), "X");
	// }

    // @Test
    // public void jsonVarTest() throws ParseException {
    //     List<Token> tokens = setup("json X");
    //     ITerm term = parser.createTerm(tokens);
    //     assertEquals(InlineVariableDeclaration.class, term.getClass());
    //     varComparison((InlineVariableDeclaration) term, new BasicType(Token.JSON), "X");
    // }

    // @Test(expected = AssertionError.class)
    // public void incorrectTypeVarTest() throws ParseException {
    //     List<Token> tokens = setup("json X");
    //     ITerm term = parser.createTerm(tokens);
    //     assertEquals(InlineVariableDeclaration.class, term.getClass());
    //     varComparison((InlineVariableDeclaration) term, new BasicType(Token.INTEGER), "X");
    // }

    // @Test(expected = AssertionError.class)
    // public void incorrectIdentifierVarTest() throws ParseException {
    //     List<Token> tokens = setup("json X");
    //     ITerm term = parser.createTerm(tokens);
    //     assertEquals(InlineVariableDeclaration.class, term.getClass());
    //     varComparison((InlineVariableDeclaration) term, new BasicType(Token.JSON), "Y");
    // }

    // @Test
    // public void objectVarTest() throws ParseException {
    //     List<Token> tokens = setup("java.lang.Object X");
    //     ITerm term = parser.createTerm(tokens);
    //     assertEquals(InlineVariableDeclaration.class, term.getClass());
    //     varComparison((InlineVariableDeclaration) term, new ObjectType(Token.OBJECT,"java.lang.Object"), "X");
    // }

    // private void varComparison(InlineVariableDeclaration dec, IType type, String id) {
	// 	assertEquals("Type mismatch", type, dec.type());
	// 	assertEquals("Identifier mismatch", id, dec.identifier());
	// }
}
