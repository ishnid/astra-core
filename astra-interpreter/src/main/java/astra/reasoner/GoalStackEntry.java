package astra.reasoner;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import astra.formula.Formula;
import astra.formula.Goal;
import astra.reasoner.util.Utilities;
import astra.term.Term;

public class GoalStackEntry implements ReasonerStackEntry {
	Goal goal;
	Queue<Formula> options = new LinkedList<Formula>();
	boolean solved = false;
	List<Map<Integer, Term>> solutions = new LinkedList<Map<Integer, Term>>();
	Map<Integer, Term> initial;
	ResolutionBasedReasoner reasoner;
	
	public GoalStackEntry(ResolutionBasedReasoner reasoner, Goal goal, Map<Integer, Term> bindings) {
		this.goal = goal;
		this.initial = bindings;
		this.reasoner = reasoner;
		
		// Generate matching formulae
		for (Queryable source : reasoner.sources) {
			source.addMatchingFormulae(options, goal);
		}
	}
	
	public boolean solve() {
		// Try to match the goal to an existing goal...
		while (!options.isEmpty()) {
			Goal option = (Goal) options.poll();
			Map<Integer, Term> bindings = Unifier.unify(goal, option, reasoner.agent);
			if (bindings != null) {
				reasoner.propagateBindings(Utilities.merge(initial, bindings));
				solved = true;
			}			
		}
		
		reasoner.stack.pop();
		return solved;
	}

	public String toString() {
		return "[GoalStackEntry] solving: " + goal.toString() + " / " + options.size();
	}

	@Override
	public boolean failure() {
		System.out.println("[GoalStackEntry]: Failure: " + goal + " / " + options.size() + " / " + solutions.size());
		if (options.isEmpty() && solutions.isEmpty()) return true;
		// Do nothing, and propagate failure (this is called when a
		// predicate cannot be solved)
		return false;
	}

	@Override
	public void addBindings(Map<Integer, Term> bindings) {
//		System.out.println("[PredicateStackEntry]: Propogating bindings: " + bindings);
		solutions.add(bindings);
	}
}
