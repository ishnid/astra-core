package astra.reasoner.util;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import astra.formula.AND;
import astra.formula.Comparison;
import astra.formula.Formula;
import astra.formula.Inference;
import astra.formula.Predicate;
import astra.reasoner.Queryable;
import astra.reasoner.ResolutionBasedReasoner;
import astra.term.Primitive;
import astra.term.Term;
import astra.term.Variable;
import astra.type.Type;

public class Test implements Queryable {
	List<Formula> formulae;
	public Test() {
		formulae = new LinkedList<Formula>();
		formulae.add(new Predicate("miner", new Term[] {Primitive.newPrimitive("B"), Primitive.newPrimitive(0), Primitive.newPrimitive(0)}));
		formulae.add(new Predicate("miner", new Term[] {Primitive.newPrimitive("A"), Primitive.newPrimitive(3), Primitive.newPrimitive(0)}));
		formulae.add(new Inference(new Predicate("option", new Term[] {new Variable(Type.STRING, "X"), new Variable(Type.INTEGER, "tot"), new Variable(Type.INTEGER, "qty")}),
				new AND(
						new Predicate("miner", new Term[] {new Variable(Type.STRING, "X"), new Variable(Type.INTEGER, "tot"), new Variable(Type.INTEGER, "qty")}),
						new Comparison(Comparison.GREATER_THAN, new Variable(Type.INTEGER, "tot"), new Variable(Type.INTEGER, "qty"))
				)
		));
	}
	
	@Override
	public void addMatchingFormulae(Queue<Formula> queue, Formula predicate) {
		queue.addAll(formulae);
	}
	
	public static void main(String[] args) {
		ResolutionBasedReasoner reasoner = new ResolutionBasedReasoner(null);
//		EISAgent agt = new EISAgent(null);
//		agt.addPercept(new Percept("square", new Identifier("forward"), new Identifier("dusty")));
//		agt.completed();
		reasoner.addSource(new Test());
//		reasoner.addSource(agt);
//		List<Map<Integer, Term>> bindings = reasoner.query(new AND(
//				new Predicate("check", new Term[] {new Variable(Type.STRING, "X"), new Variable(Type.STRING, "Y")}),
//				new Comparison(Comparison.NOT_EQUAL, new Variable(Type.STRING, "X"), new Variable(Type.STRING, "Y"))
//		));
		List<Map<Integer, Term>> bindings = reasoner.query(new Predicate("option", new Term[] {
				new Variable(Type.STRING, "X"), 
				new Variable(Type.INTEGER, "tot"), 
				new Variable(Type.INTEGER, "qty")})
		);
		System.out.println(bindings);
	}

}
