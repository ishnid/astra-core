package astra.statement;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import astra.core.Agent.Promise;
import astra.core.Agent;
import astra.core.Intention;
import astra.formula.Formula;
import astra.reasoner.util.ContextEvaluateVisitor;
import astra.term.Primitive;
import astra.term.Term;

public class Wait extends AbstractStatement {
	Formula guard;
	Term timeout;
	
	public Wait(String clazz, int[] data, Formula guard) {
		this(clazz, data,guard, null);
	}

	public Wait(String clazz, int[] data, Formula guard, Term timeout) {
			this.setLocation(clazz, data[0], data[1], data[2], data[3]);
		this.guard = guard;
		this.timeout = timeout;
	}
	
	public StatementHandler getStatementHandler() {
		return new AbstractStatementHandler() {
			int state = 0;
			Promise promise;
			
			@SuppressWarnings("unchecked")
			public boolean execute(final Intention intention) {
				switch (state) {
				case 1:
					state = 0;
					return true;
				case 0:
					ContextEvaluateVisitor visitor = new ContextEvaluateVisitor(intention);

					if (timeout == null) {
						/**
						 * State Promise
						 */
						promise=new Promise() {
							List<Map<Integer, Term>> bindings;
							Formula formula = (Formula) guard.accept(visitor);

							public boolean evaluate(Agent agent) {
								return (bindings = agent.query(formula, new HashMap<Integer, Term>())) != null;
							}

							public void act() {
								if (bindings != null) executor.addBindings(bindings.get(0));
								intention.resume();
							}
						};
					} else {
						/**
						 * Time & State Promise
						 */
						promise=new Promise() {
							List<Map<Integer, Term>> bindings;
							Formula formula = (Formula) guard.accept(visitor);
							long delay = ((Primitive<Integer>) timeout.accept(visitor)).value();
							long startTime = System.currentTimeMillis();

							public boolean evaluate(Agent agent) {
								bindings = agent.query(formula, new HashMap<Integer, Term>());
								long duration = System.currentTimeMillis()-startTime;
								return (bindings != null) || (duration >= delay);
							}

							public void act() {
								if (bindings != null) executor.addBindings(bindings.get(0));
								intention.resume();
							}
						};
					}
					intention.makePromise(promise);
					intention.suspend();

					state = 1;
				}
				return false;
			}

			public boolean onFail(Intention intention) {
				intention.dropPromise(promise);
				return false;
			}

			public Statement statement() {
				return Wait.this;
			}
			
		};
	}

}
