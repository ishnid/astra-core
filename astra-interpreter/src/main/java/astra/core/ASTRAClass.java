package astra.core;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;

import astra.event.Event;
import astra.formula.Formula;
import astra.formula.Inference;
import astra.formula.Predicate;
import astra.reasoner.Queryable;
import astra.term.Term;
import astra.tr.Function;

public abstract class ASTRAClass implements Queryable {
	Map<String, List<Rule>> rules = new HashMap<>();
	Map<Integer, Function> functions = new HashMap<>();
	Map<Integer, List<Formula>> inferences = new HashMap<>();
	private Set<String> filter = new HashSet<>();

	private List<ASTRAClass> linearization;
	private Class<ASTRAClass>[] parents;
	private int distFromRoot = -1;

	public void setParents(Class<ASTRAClass>[] parents) {
		this.parents = parents;
	}

	public synchronized Agent newInstance(String name) throws AgentCreationException, ASTRAClassNotFoundException {
		if (Agent.hasAgent(name)) {
			throw new AgentCreationException("An agent with name: \"" + name + "\" already exists.");
		}
		
		Agent agent = new Agent(name);
		agent.setMainClass(this);
		
		return agent;
	}
	
	public abstract void initialize(Agent agent);
	
	public abstract Fragment createFragment(Agent agent) throws ASTRAClassNotFoundException;
	
	public List<ASTRAClass> getLinearization() throws ASTRAClassNotFoundException {
		if (linearization==null) {
			linearization = new LinkedList<>();
			
			Queue<ASTRAClass> queue = new PriorityQueue<>(1, (o1,o2) -> o1.getDistance()-o2.getDistance());
			
			Queue<ASTRAClass> queue2 = new LinkedList<>();
			queue2.add(this);
			while (!queue2.isEmpty()) {
				ASTRAClass claz = queue2.poll();
				if (claz.parents != null) {
					for (int i=claz.parents.length-1; i>-1; i--) {
						ASTRAClass c = ASTRAClassLoader.getDefaultClassLoader().loadClass(claz.parents[i]);
						if (!queue.contains(c) && !queue2.contains(c)) {
							queue2.add(c);
						}
					}
					queue.add(claz);
				}
			}
			
			while (!queue.isEmpty()) {
				ASTRAClass claz = queue.poll();
				if (!linearization.contains(claz)) 
					linearization.add(0, claz);
			}
		}
		return linearization;
	}
	
	public int getDistance() {
		int maxDist;
		int d;
		if (distFromRoot==-1) {
			maxDist=0;
			if (parents != null) {
				for (Class<ASTRAClass> parent : this.parents) {
					try {
						ASTRAClass cls = ASTRAClassLoader.getDefaultClassLoader().loadClass(parent);
						d = cls.getDistance();
						if (d > maxDist) maxDist = d;
					} catch (ASTRAClassNotFoundException e) {
						e.printStackTrace();
					}
				}
			}

			distFromRoot = maxDist+1;
		}
		return distFromRoot;
	}
	
	public String toString() {
		return "ASTRAClass:"+getClass().getCanonicalName();
	}
	
	public String getCanonicalName() {
		return getClass().getCanonicalName();
	}
	
	public void addRule(Rule rule) {
		List<Rule> list = rules.get(rule.event.signature());
		if (list == null) {
			filter.add(rule.event.signature());
			list = new LinkedList<>();
			rules.put(rule.event.signature(), list);
		}
		
		list.add(rule);
	}
	
	public void addInference(Inference inference) {
		List<Formula> list = inferences.get(inference.head().id());
		if (list == null) {
			list = new LinkedList<>();
			inferences.put(inference.head().id(), list);
		}
		
		list.add(inference);
	}
	
	public void addFunction(Function function) {
		if (functions.containsKey(function.identifier.id())) {
			System.out.println("Attempt to add duplicate function :" + function.identifier);
			return;
		}
		functions.put(function.identifier.id(), function);
	}
	
	public Set<String> filter() {
		return filter;
	}

	public boolean handleEvent(Event event, Agent agent) {
		if (agent.trace()) System.out.println("["+agent.name()+"] Checking class: " + getClass().getCanonicalName());
		List<Rule> list = rules.get(event.signature());
		if (list == null) return false;
		
		for (Rule rule : list) {
			if (agent.trace()) System.out.println("["+agent.name()+"] \tChecking rule: " + rule.event);
			Map<Integer, Term> bindings = Helper.evaluateRule(agent, rule, event);
			if (bindings != null) {
				Object source = event.getSource();
				if (source != null) {
					Intention intention = null;
					if (Intention.class.isInstance(source)) {
						intention = (Intention) source;
						intention.addSubGoal(event, rule, bindings, null);
						if (agent.trace()) System.out.println("["+agent.name()+"] \tADDING SUBGOAL: " + rule.event);
					} else if (RuleExecutor.class.isInstance(source)) {
						RuleExecutor executor = (RuleExecutor) source;
						intention = executor.intention();
						intention.addSubGoal(event, rule, bindings, executor);
						if (agent.trace()) System.out.println("["+agent.name()+"] \tADDING RULE SUBGOAL: " + rule.event);
					}	
					intention.resume();
				} else {
					if (agent.trace()) System.out.println("["+agent.name()+"] \tCREATING NEW INTENTION: " + rule.event);
					agent.addIntention(new Intention(agent, event, rule, bindings));
				}
				return true;
			}
		}
		
		return false;
	}
	
	public static ASTRAClass forName(String url) throws ASTRAClassNotFoundException {
		return ASTRAClassLoader.getDefaultClassLoader().loadClass(url);
	}
	
	public static ASTRAClass forName(String _package, String url) throws ASTRAClassNotFoundException {
		if (_package == null) return forName(url);
		return ASTRAClassLoader.getDefaultClassLoader().loadClass(_package+"."+url);
	}

	public Function getFunction(Predicate predicate) {
		return functions.get(predicate.id());
	}

	public boolean hasFunctions() {
		return !functions.isEmpty();
	}

	public void addMatchingFormulae(Queue<Formula> queue, Formula predicate) {
		if (predicate instanceof Predicate) {
			List<Formula> list = inferences.get(((Predicate) predicate).id());
			if (list != null) queue.addAll(list);
		}
	}

	public boolean isSubclass(ASTRAClass cl) {
		try {
			for (ASTRAClass cls : cl.getLinearization()) {
				if (cls.getCanonicalName().equals(this.getCanonicalName())) return true;
			}
		} catch (ASTRAClassNotFoundException e) {
			e.printStackTrace();
		}
		return false;
	}

	public Map<String, List<Rule>> rules() {
		return this.rules;
	}

	public Class<ASTRAClass>[] getParents() {
		return parents;
	}
}
