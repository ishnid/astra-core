package astra.event;

import astra.formula.Predicate;
import astra.reasoner.util.LogicVisitor;

public class ModuleEvent implements Event {
	public String module;
	public String signature;
	public Predicate event;
	public ModuleEventAdaptor adaptor;

	public ModuleEvent(String module, String signature, Predicate belief, ModuleEventAdaptor adaptor) {
		this.module = module;
		this.signature = signature;
		this.event = belief;
		this.adaptor = adaptor;
	}

	public String module() {
		return module;
	}

	public Predicate event() {
		return event;
	}

	public ModuleEventAdaptor adaptor() {
		return adaptor;
	}

	public Object getSource() {
		return null;
	}

	public String toString() {
		return "$" + module + "." + event.toString();
	}

	public String signature() {
		return signature;
	}

	@Override
	public Event accept(LogicVisitor visitor) {
		return new ModuleEvent(module, signature, (Predicate) event.accept(visitor), adaptor);
	}
}
