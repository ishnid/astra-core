package astra.statement;

import java.util.Queue;

import astra.core.RuleExecutor;
import astra.formula.Formula;

public abstract class AbstractStatementHandler implements StatementHandler {
	protected RuleExecutor executor;
	
	public void addGoals(Queue<Formula> list) {
		// Do nothing by default...
	}

	public void setRuleExecutor(RuleExecutor executor) {
		this.executor = executor;
	}

}
